<?php

namespace Drupal\elf;

/**
 * Provides an interface defining the elf manager.
 */
interface ElfManagerInterface {

  /**
   * Creates a safe internal URL that redirects to the an external one.
   *
   * @param string|\Drupal\Core\Url $external_url
   *   The target URL.
   *
   * @return \Drupal\Core\Url
   *   An internal URL that points to the ELF redirection controller.
   */
  public function getRedirectUrl($external_url);

}
