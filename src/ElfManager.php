<?php

namespace Drupal\elf;

use Drupal\Core\PrivateKey;
use Drupal\Core\Url;
use Drupal\Component\Utility\Crypt;

/**
 * Defines the ELF manager.
 */
class ElfManager implements ElfManagerInterface {

  /**
   * The private key.
   *
   * @var \Drupal\Core\PrivateKey
   */
  protected $privateKey;

  /**
   * Constructs a new ElfManager object.
   *
   * @param \Drupal\Core\PrivateKey $private_key
   *   The private key.
   */
  public function __construct(PrivateKey $private_key) {
    $this->privateKey = $private_key;
  }

  /**
   * {@inheritdoc}
   */
  public function getRedirectUrl($external_url) {
    $key = Crypt::hmacBase64(
      is_string($external_url) ? $external_url : $external_url->toString(),
      $this->privateKey->get());

    return Url::fromRoute('elf.redirect', [], [
      'query' => [
        'url' => $external_url,
        'key' => $key,
      ],
    ]);
  }

}
