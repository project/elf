<?php

namespace Drupal\elf\Plugin\Filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Component\Utility\Html;

/**
 * Adds a CSS class to all external and mailto links.
 *
 * @Filter(
 *   id = "filter_elf",
 *   title = @Translation("Add an icon to external and mailto links"),
 *   description = @Translation("External and mailto links in content links have an icon."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE,
 *   settings = {
 *     "elf_nofollow" = false,
 *     "elf_noopener" = false,
 *     "elf_noreferrer" = false
 *   }
 * )
 */
class FilterElf extends FilterBase {

  /**
   * Store internal domains.
   *
   * @var string
   */
  protected $pattern = '';

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form['elf_nofollow'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add rel="nofollow" to all external links'),
      '#default_value' => $this->settings['elf_nofollow'] ?? FALSE,
    ];
    $form['elf_noopener'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add rel="noopener" to all external links'),
      '#default_value' => $this->settings['elf_noopener'] ?? FALSE,
    ];
    $form['elf_noreferrer'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add rel="noreferrer" to all external links'),
      '#default_value' => $this->settings['elf_noreferrer'] ?? FALSE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $elf_settings = \Drupal::config('elf.settings');
    $result = new FilterProcessResult($text);
    $icon_class = $elf_settings->get('elf_icon_class');

    $dom = Html::load($text);
    $links = $dom->getElementsByTagName('a');
    foreach ($links as $a) {
      $href = $a->getAttribute('href');
      if (!$href) {
        continue;
      }
      // This is a mailto link.
      if (strpos($href, 'mailto:') === 0) {
        $a->setAttribute('class', ($a->getAttribute('class') ? $a->getAttribute('class') . ' elf-mailto ' . $icon_class : 'elf-mailto ' . $icon_class));
        continue;
      }
      // This is external links.
      if ($this->elfUrlExternal($href)) {
        // The link is external. Add external class.
        $a->setAttribute('class', ($a->getAttribute('class') ? $a->getAttribute('class') . ' elf-external ' . $icon_class : 'elf-external ' . $icon_class));
        if ($a->getElementsByTagName('img')->length > 0) {
          $a->setAttribute('class', ($a->getAttribute('class') ? $a->getAttribute('class') . ' elf-img' : 'elf-img'));
        }

        // Open a link in a new tab.
        if ($elf_settings->get('elf_window')) {
          $a->setAttribute('target', "_blank");
        }

        // Make external link indication accessible.
        if ($elf_settings->get('elf_accessible')) {
          $accessible_label = $dom->createElement(
            'span',
            $elf_settings->get('elf_window') ? 'external link, opens in a new tab' : 'external link'
          );
          $accessible_label->setAttribute('class', 'screen-reader-only');
          $a->appendChild($accessible_label);
        }

        // Add rel options.
        $rel_options = [
          'elf_nofollow' => 'nofollow',
          'elf_noopener' => 'noopener',
          'elf_noreferrer' => 'noreferrer',
        ];
        $rel = array_filter(explode(' ', $a->getAttribute('rel')));
        foreach ($rel_options as $rel_key => $rel_value) {
          $rel_option_enabled = $this->getConfiguration()['settings'][$rel_key];
          if ($rel_option_enabled) {
            if (!in_array($rel_value, $rel)) {
              $rel[] = $rel_value;
            }
          }
        }
        $a->setAttribute('rel', implode(' ', $rel));

        // Add redirect.
        if ($elf_settings->get('elf_redirect')) {
          $redirect_url = \Drupal::service('elf.manager')
            ->getRedirectUrl($a->getAttribute('href'));
          $a->setAttribute('href', $redirect_url->toString());
        }
      }
    }

    $result->setProcessedText(Html::serialize($dom))
      ->addAttachments(['library' => ['elf/elf_css']]);

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    return $this->t('External and mailto links in content links have an icon.');
  }

  /**
   * Check external links.
   *
   * @param string $url
   *   Url.
   *
   * @return bool
   *   Check url is external or not.
   */
  public function elfUrlExternal($url) {
    global $base_url;

    if (empty($this->pattern)) {
      $domains = [];
      $elf_domains = \Drupal::config('elf.settings')->get('elf_domains');
      $elf_domains = is_array($elf_domains) ? $elf_domains : [];
      foreach (array_merge($elf_domains, [$base_url]) as $domain) {
        $domains[] = preg_quote($domain, '#');
      }
      $this->pattern = '#^(' . str_replace('\*', '.*', implode('|', $domains)) . ')#';
    }

    return preg_match($this->pattern, $url) ? FALSE : UrlHelper::isExternal($url);
  }

}
